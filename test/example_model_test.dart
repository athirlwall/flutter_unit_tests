import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:flutter_unit_tests/main.dart';
import 'package:flutter_unit_tests/person_model.dart';
import 'package:flutter_unit_tests/person_collection.dart';

void main() {
  test('Test Person', () {
    Map<String, dynamic> personMap = {
      'name': 'Alex Thirlwall',
      'age': 41
    };

    Person person = Person.fromJson(personMap);

    // Tests Person.fromJson static method
    expect(person.name, equals('Alex Thirlwall'));
    expect(person.age, equals(41));
  });

  test('Test Person with JSON decoder', () {

    // I am just including this example to show the steps
    // between reading the JSON string and constructing the
    // final model, via the intermediate step of producing
    // the Map<String, dynamic> which represents a decoded
    // JSON object ... there is no real need to test the 
    // json.decode command since preumably the package
    // authors have done that already (we hope!)

    String rawJson = """
{
  "name": "John Smith",
  "age": 54
}
""";

    // Produces the Map<String, dynamic> object
    Map<String, dynamic> decodedJson = json.decode(rawJson);

    // Parses the Map<String, dynamic> object into a Person Model
    Person person = Person.fromJson(decodedJson);

    // Check that the Model actually contains the desired properties
    expect(person.name, equals('John Smith'));
    expect(person.age, equals(54));
  });

  test('Test a list of Persons',() {
      List<dynamic> personsList = [
        {'name': 'Henk Moeders', 'age': 88},
        {'name': 'Annie Lowels', 'age': 45}
      ];

      // Creates a PersonCollection with an empty list of persons
      PersonCollection personCollection = PersonCollection.fromJson(personsList);

      // Tests PersonCollection.fromJson, PersonCollection.add and Person.fromJson methods
      expect(personCollection.persons[0].name, 'Henk Moeders');
      expect(personCollection.persons[1].name, 'Annie Lowels');
      expect(personCollection.persons[0].age, 88);
      expect(personCollection.persons[1].age, 45);

      personCollection.removeLast();

      // Tests removeLast method
      expect(personCollection.persons.length, 1);
  });
}