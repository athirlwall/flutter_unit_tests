import 'person_model.dart';

class PersonCollection {
  List<Person> persons = [];

  PersonCollection(this.persons);

  void add(Person person) {
    this.persons.add(person);
  }

  void removeLast() {
    this.persons.removeLast();
  }

  void clear() {
    this.persons.clear();
  }

  static PersonCollection fromJson(List<dynamic> decodedJson) {
    List<Person> persons = [];
    decodedJson.forEach((person) {
      persons.add(Person.fromJson(person));
    });
    return PersonCollection(persons);
  }
}