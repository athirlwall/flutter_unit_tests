class Person {
  String name;
  int age;

  Person({this.name, this.age});

  static Person fromJson(Map<String, dynamic> decodedPerson) {
    return Person(name: decodedPerson['name'], age: decodedPerson['age']);
  }
}