import 'person_model.dart';

class Address {
  String address1;
  String postcode;
  int house_humber;
  int house_extension;
  Person girlfriend;

  Address({
    this.address1,
    this.postcode,
    this.house_humber,
    this.house_extension,
    this.girlfriend
  });

  static Address fromJson(Map<String, dynamic> decodedJson) {
    
  }
}